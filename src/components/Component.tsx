import { Component } from "react";
import axios from "axios";

export class InputClass extends Component{
  
  state = { url: "", response: "", error: "" };

  render() {   
  
    return (
      <>
        <label>
          URL: <input value={this.state.url} onChange={(e) => 
            {
              this.setState((state) => ({
                response: ""
              }));
              this.setState((state) => ({
                error: ""
              }));
              this.setState((state) => ({
                url: e.target.value
              }));
            }}/>
        </label>
  
        <p>
          <button onClick={async () => { 
            
            this.setState((state) => ({
              response: ""
            }));
            this.setState((state) => ({
              error: ""
            }));
            
            await axios.get(this.state.url)
            .then(response => {

              this.setState((state) => ({
                response: JSON.stringify(response.data)
              }));
            })
            .catch(error => {

              this.setState((state) => ({
                error: error.message
              }));
            });

            
            }} disabled={!this.state.url} >Отправить</button>
        </p>
        <div>
          {this.state.response}
        </div>
        <div>
          <p style={{ color: 'red' }}>{this.state.error}</p>
        </div>
      </>
    );
  }
}


